package kernel

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
)

func WriteEvent(wr io.Writer, event interface{}) error {
	data, err := json.Marshal(event)
	if err != nil {
		return err
	}

	_, err = fmt.Fprintf(wr, "Content-Length: %d\r\n\r\n%s\r\n", len(data)+2, data)
	return err
}

type EventReader struct {
	rd  io.Reader
	buf [1 << 10]byte
	err error
	ch  chan interface{}
}

func NewEventReader(rd io.Reader) *EventReader {
	e := new(EventReader)
	e.rd = rd
	e.ch = make(chan interface{})
	go e.read()
	return e
}

func (e *EventReader) read() {
	defer close(e.ch)

	var buf [1 << 10]byte
	var pos int
	headers := map[string]string{}
read:
	for {
		n, err := e.rd.Read(buf[pos:])
		if err != nil {
			e.err = err
			return
		}
		if n == 0 {
			e.err = errors.New("read zero bytes")
			return
		}
		pos += n

	header:
		var i, j int
		for {
			j = bytes.Index(buf[i:pos], []byte("\r\n"))
			if j < 0 {
				continue read // Wait for more data
			}
			if j == 0 {
				i += 2
				break
			}

			parts := bytes.SplitN(buf[i:i+j], []byte(":"), 2)
			headers[strings.ToLower(strings.TrimSpace(string(parts[0])))] = strings.TrimSpace(string(parts[1]))
			i += j + 2
		}

		length, err := strconv.ParseUint(headers["content-length"], 10, 64)
		if err != nil {
			log.Printf("Content length is missing or invalid: %q", headers["content-length"])
			copy(buf[:], buf[i:pos])
			pos -= i
			continue
		}

		b := make([]byte, length)
		n = copy(b, buf[i:pos])
		i += n
		if uint64(n) < length {
			_, err = io.ReadFull(e.rd, b[n:])
			if err != nil {
				e.err = err
				return
			}
		}

		event, err := UnmarshalEvent(b)
		if err != nil {
			log.Printf("Invalid event: %v", err)
			continue
		}

		e.ch <- event
		copy(buf[:], buf[i:pos])
		pos -= i
		if pos > 0 {
			goto header
		}
	}
}

func (e *EventReader) Read() (interface{}, error) {
	v, ok := <-e.ch
	if !ok {
		return nil, e.err
	}
	return v, nil
}
