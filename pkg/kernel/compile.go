package kernel

import (
	"context"
	"errors"
	"fmt"
	"go/ast"
	"go/parser"
	"go/scanner"
	"go/token"

	"gitlab.com/ethan.reesor/vscode-notebooks/yaegi/interp"
)

func (k *Kernel) parse(code string) (ast.Node, error) {
	const mode = parser.DeclarationErrors
	const filename = "_.go"

	// Use a new fileset for the scanner
	var scanner scanner.Scanner
	fset := k.interp.FileSet()
	file := fset.AddFile("", fset.Base(), len(code))
	scanner.Init(file, []byte(code), nil, 0)

	// Use the interpreter's fileset to parse
	_, tok, _ := scanner.Scan()
	switch tok {
	case token.PACKAGE:
		// If the first token is `package`, it must be a whole file
		return parser.ParseFile(k.interp.FileSet(), filename, code, mode)

	case token.IMPORT, token.FUNC:
		// If the first token is `import` or `func`, it must be a whole file
		// minus the package declaration
		return parser.ParseFile(k.interp.FileSet(), filename, fmt.Sprintf(`
			package main

			%s
		`, code), mode)

	case token.CONST, token.TYPE, token.VAR:
		// Parse at the global scope
		file, err := parser.ParseFile(k.interp.FileSet(), filename, fmt.Sprintf(`
			package main

			%s
		`, code), mode)
		if err == nil {
			return file, nil
		}

		// Try again within a main function
		fallthrough

	default:
		// Parse within a main function
		file, err := parser.ParseFile(k.interp.FileSet(), filename, fmt.Sprintf(`
			package main

			func main() {
				%s
			}
		`, code), mode)
		if err != nil {
			return nil, err
		}

		// Return the body of the main function
		return file.Decls[0].(*ast.FuncDecl).Body, nil
	}
}

func shouldShowResult(node ast.Node) bool {
	switch node := node.(type) {
	case *ast.BlockStmt:
		return shouldShowResult(node.List[len(node.List)-1])

	case *ast.ExprStmt:
		return shouldShowResult(node.X)

	case *ast.ParenExpr:
		return shouldShowResult(node.X)

	case *ast.Ident,
		*ast.BasicLit,
		*ast.CompositeLit,
		*ast.SelectorExpr,
		*ast.IndexExpr,
		// *ast.IndexListExpr,
		*ast.SliceExpr,
		*ast.TypeAssertExpr,
		*ast.StarExpr,
		*ast.UnaryExpr,
		*ast.BinaryExpr:
		// Show simple values and unary and binary expressions, but do not show
		// the result of a function call
		return true

	default:
		return false
	}
}

func (k *Kernel) compile(id int, code string) *session {
	node, err := k.parse(code)
	if err != nil {
		k.sendErr(id, err, nil)
		k.SendSafe(Executed(id, false, 0, nil, false))
		return nil
	}

	prog, err := k.interp.CompileAST(node)
	if err != nil {
		k.sendErr(id, err, nil)
		k.SendSafe(Executed(id, false, 0, nil, false))
		return nil
	}

	s := new(session)
	s.kernel = k
	s.id = id
	s.program = prog
	s.showResult = shouldShowResult(node)
	s.context, s.cancel = context.WithCancel(k.context)
	return s
}

func (k *Kernel) sendErr(id int, err error, abort *didAbort) {
	if errors.Is(err, context.Canceled) {
		if abort != nil {
			if k.Notebook.CanAbort {
				k.SendSafe(Abort(id, abort.value...).
					WithStack(string(abort.stack)))
			} else {
				k.SendSafe(Error(id, fmt.Sprint(abort.value...)).
					WithName("Aborted").
					WithStack(string(abort.stack)))
			}
		}
		return
	}

	var errPanic interp.Panic
	if errors.As(err, &errPanic) {
		k.SendSafe(Error(id, fmt.Sprint(errPanic.Value)).
			WithName("Panic").
			WithStack(string(errPanic.Stack)))
		return
	}

	var errList scanner.ErrorList
	if errors.As(err, &errList) {
		for _, err := range errList {
			var errScanner *scanner.Error
			evt := Error(id, err.Error()).WithName("ParserError")
			if errors.As(err, &errScanner) {
				evt.WithLine(errScanner.Pos.Line, errScanner.Pos.Column)
			}
			k.SendSafe(evt)
		}
		return
	}

	k.SendSafe(Error(id, err.Error()))
}
