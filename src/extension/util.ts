import fs = require('fs/promises')
import os = require('os')
import path = require('path')
import { ChildProcess } from 'child_process'
import kill = require('tree-kill')
import { Resources } from './main'
import { Uri } from 'vscode'
const { F_OK, X_OK } = require('fs').constants

let tmpDir: string | undefined
const destroyer = {
    async dispose() {
        if (tmpDir) {
            await rmdir(tmpDir)
        }
        tmpDir = undefined
    }
}

export async function getTempFilePath(name: string): Promise<string> {
	if (!tmpDir) {
        Resources.register(destroyer)
        tmpDir = await fs.mkdtemp(path.join(os.tmpdir(), 'go-playbooks'))
	}

    if (!await exists(tmpDir)) {
        await fs.mkdir(tmpDir)
    }

	return path.normalize(path.join(tmpDir, name))
}

async function canAccess(filename: string, mode: number) {
    try {
        await fs.access(filename, mode)
        return true
    } catch (error) {
        return false
    }
}

export function exists(filename: string) {
    return canAccess(filename, F_OK)
}

export function canExecute(filename: string) {
    return canAccess(filename, X_OK)
}

async function rmdir(dir: string) {
    if (!await exists(dir)) {
        return
    }

    for (const file of await fs.readdir(dir)) {
        const filename = path.join(dir, file)
        if ((await fs.lstat(filename)).isDirectory()) {
            await rmdir(filename)
            continue
        }

        await fs.unlink(filename).catch((err) => {
            console.log(`failed to remove ${filename}: ${err}`)
        })
    }

    await fs.rmdir(dir)
}


// Kill a process and its children, returning a promise.
//
// Taken from https://github.com/golang/vscode-go/blob/8c5171ddd6e6e4c03524b3b723f431a814bd14cc/src/utils/processUtils.ts.
// Copyright 2020 The Go Authors. All rights reserved. Licensed under the MIT License.
export function killProcessTree(p: ChildProcess): Promise<void> {
	if (!p || !p.pid || p.exitCode !== null) {
		return Promise.resolve()
	}
	return new Promise((resolve) => {
		kill(p.pid, (err) => {
			if (err) {
                console.log(`failed to kill process ${p.pid}: ${err}`)
			}
			resolve()
		})
	})
}