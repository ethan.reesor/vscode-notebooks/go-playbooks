import crypto = require('crypto')
import fs = require('fs/promises')
import path = require('path')
import which = require('which')
import yaml = require('yaml')
import { createServer, Server, Socket } from 'net'
import { ChildProcess, exec, spawn } from 'child_process'
import { Duplex } from 'stream'
import { commands, debug, Disposable, EventEmitter, extensions, NotebookCell, NotebookDocument, Uri, window, workspace } from 'vscode'
import { canExecute, exists, getTempFilePath, killProcessTree } from './util'
import { Resources } from './main'
import * as Go from './vscode-go'
import { NotebookCellMetadata } from '../vscode-markdown-notebook/markdownParser'


const output = window.createOutputChannel('Go Playbook Kernel')

function parseFrontmatter(cell: NotebookCell): any {
    const metadata = cell.metadata as NotebookCellMetadata
    if (!metadata.isFrontmatter) {
        return
    }

    try {
    	return yaml.parse(cell.document.getText())
    } catch (_) {
        return
    }
}

export class UnixSocketProcess {
    public static extensionPath: string | undefined

    constructor(
        private readonly doc: NotebookDocument,
    ){
        Resources.register(this)
    }

    private readonly server: Server = createServer()
    private socketFile: string | undefined
    private process: ChildProcess | undefined
    private socket: Socket | undefined

    private static async getInvocation(doc: NotebookDocument, frontmatter: any) {
        let tool: Go.ToolAtVersion = {
            name: 'go-playbook-kernel',
            importPath: 'gitlab.com/ethan.reesor/vscode-notebooks/go-playbooks/cmd/go-playbook-kernel',
            modulePath: 'gitlab.com/ethan.reesor/vscode-notebooks/go-playbooks',
            isImportant: true,
            description: 'Go Playbook Kernel',
        }
        if (frontmatter?.engine?.name) {
            tool = frontmatter.engine
        }

        // Check the path for the executable
        const execPath = await which(tool.name).catch(() => null)
        if (execPath) {
            return { binPath: execPath }
        }

        if (!tool.importPath || !tool.modulePath) {
            window.showErrorMessage('Binary not found. Engine declaration does not include installation information.')
            return
        }

        // Is the Go extension installed?
        const goExt = extensions.getExtension<Go.ExtensionAPI>('golang.go') ||
            extensions.getExtension<Go.ExtensionAPI>('golang.go-nightly')
        if (!goExt) {
            window.showErrorMessage('Binary not found. Install the binary or install the Go extension.')
            return
        }

        // Ask Go to find the executable
        const goApi = await goExt.activate()
        let invocation = goApi.settings.getExecutionCommand(tool.name, doc.uri)
        if (invocation && await canExecute(invocation.binPath)) {
            return invocation
        }

        // Ask the user if we should install the executable
        if (!await window.showWarningMessage('Cannot find the kernel', 'Install')) {
            return
        }

        // Ask Go to install the executable
        await commands.executeCommand('go.tools.install', [tool])

        // Ask Go to find the executable
        invocation = goApi.settings.getExecutionCommand(tool.name, doc.uri)
        if (invocation && await canExecute(invocation.binPath)) {
            return invocation
        }

        window.showErrorMessage('Unable to locate the kernel.')
    }

    async start() {
        // Create a socket file name
        const socketFile = await getTempFilePath(`exec-server-${crypto.randomBytes(16).toString('hex')}.socket`)
        this.socketFile = socketFile

        // Set up a promise
        let resolve: (_: Socket) => void
        let reject: (_?: any) => void
        const socket = new Promise<Socket>((r, j) => (resolve = r, reject = j))

        // Listen
        await new Promise<void>(r => this.server.listen(socketFile, r))
        this.server.on('connection', x => resolve(x))
        crypto.randomBytes(16).toString('hex')

        const frontmatter = parseFrontmatter(this.doc.cellAt(0))
        const debugInfo = frontmatter?.engine?.debug as { enable?: boolean, source?: string } | undefined
        const debugPath = debugInfo?.source || (UnixSocketProcess.extensionPath && path.join(UnixSocketProcess.extensionPath, 'cmd', 'go-playbook-kernel'))

        // Start the process
        const dir = path.dirname(this.doc.uri.fsPath)
        if (debugInfo?.enable && debugPath) {
            const started = await debug.startDebugging(workspace.getWorkspaceFolder(this.doc.uri), {
                name: 'Run Kernel',
                type: 'go',
                request: 'launch',
                mode: 'auto',
                program: path.isAbsolute(debugPath) ? debugPath : path.join(dir, debugPath),
                cwd: dir,
                args: [socketFile]
            })
            if (!started) {
                return
            }
        } else {
            const invocation = await UnixSocketProcess.getInvocation(this.doc, frontmatter)
            if (!invocation) {
                return
            }
            if (!invocation.args) {
                invocation.args = []
            }
            invocation.args.push(socketFile)

            this.process = spawn(invocation.binPath, invocation.args, {
                cwd: dir,
                env: invocation.env as { [key: string]: string | undefined },
            })
            this.process.stdout?.on('data', c => output.append(c.toString('utf-8')))
            this.process.stderr?.on('data', c => output.append(c.toString('utf-8')))
            this.process.on('exit', (code) => {
                if (code == 0) return
                window.showErrorMessage(`Kernel exited with code ${code}`)
                output.show()
                reject(new Error(`Exited: ${code}`))
            })
            this.process.on('error', (err) => {
                window.showErrorMessage(`Kernel failed to launch: ${err}`)
                this.dispose()
                reject(new Error(`Failed to launch: ${err}`))
            })
        }

        // Wait for a connection
        this.socket = await socket

        // Close the server when the connection closes
        this.socket.on('close', () => this.stop())

        return this.socket
    }

    // Alias for stop
    async dispose() { this.stop() }

    async stop() {
        // Remove the resource for GC
        Resources.unregister(this)

        // Close the socket
        await this.socket?.destroy()

        // Kill the process
        if (this.process) {
            await killProcessTree(this.process)
        }

        // Close the server
        await new Promise(r => this.server.close(r))

        // Remove the socket file
        if (this.socketFile && await exists(this.socketFile)) {
            await fs.unlink(this.socketFile)
        }
    }
}

type EventListener<T, R> = (_: T) => R | Promise<R>

export class Stream<Recv, Send> {
    constructor(
        private readonly stream: Duplex,
    ){
        stream.on('data', x => this.recv(x))
        stream.on('close', () => this._close.fire())
    }

    private readonly eventListeners = new Set<EventListener<Recv, boolean>>()
    public onEvent(fn: EventListener<Recv, boolean>): Disposable {
        this.eventListeners.add(fn)
        const dispose = () => this.eventListeners.delete(fn)
        return { dispose }
    }

    private readonly _close = new EventEmitter<void>()
    public readonly onClose = this._close.event

    public destroy() {
        this.stream.destroy()
    }

    private data: Buffer | undefined
    private async recv(data: Buffer) {
        if (this.data?.length) {
            data = Buffer.concat([this.data, data])
        }
        this.data = data

        while (data.length > 0) {
            const headers: { [name: string]: string } = {}
            while (true) {
                // Check for CRLF
                const i = data.indexOf('\r\n')
                if (i < 0) {
                    return // Wait for more data
                }

                // Extract the line
                const raw = data.slice(0, i).toString('utf-8')
                data = data.slice(i+2)
                if (raw.length == 0) {
                    break // Header is complete
                }

                const [name, value] = raw.split(/:\s*/, 2)
                headers[name.toLowerCase()] = value
            }

            if (!('content-length' in headers)) {
                console.log('Missing content length')
                this.data = data
                return
            }

            const length = Number(headers['content-length'])
            if (isNaN(length)) {
                console.log('Invalid content length')
                this.data = data
                return
            }

            if (data.length < length) {
                return // Wait for more data
            }

            const payload = data.slice(0, length)
            this.data = data = data.slice(length)
            try {
                const event = JSON.parse(payload.toString('utf-8'))
                let handled = false
                for (const listener of this.eventListeners) {
                    if (await listener(event) !== false) {
                        handled = true
                        break
                    }
                }
                if (!handled) {
                    console.log('Received (unhandled)', event)
                }
            } catch (error) {
                console.log(`Invalid content: ${error}`)
            }
        }
    }

    public send(event: Send) {
        // console.log('Sending', event)
        const raw = Buffer.from(JSON.stringify(event)+'\r\n', 'utf-8')
        const header = Buffer.from(`Content-Length: ${raw.length}\r\n\r\n`, 'utf-8')
        this.stream.write(Buffer.concat([header, raw]))
    }
}