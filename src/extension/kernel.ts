import { Socket } from "net"
import { commands, Diagnostic, DiagnosticSeverity, Disposable, EventEmitter, languages, NotebookCell, NotebookCellExecution, NotebookCellOutput, NotebookCellOutputItem, NotebookController, NotebookDocument, Range, TextDocument, Uri, window, workspace } from "vscode"
import { Stream, UnixSocketProcess } from "./stream"

class NotebookCapabilities {
    readonly type = 'capabilities'
    canAbort = true
}

class InterruptRequest {
    readonly type = 'interrupt'
}

class ExecuteRequest {
    private static id = 1
    readonly id = ExecuteRequest.id++
    readonly type = 'execute'

    constructor(
        readonly code: string,
    ){}
}

class VariableRequest {
    private static id = 1
    readonly id = VariableRequest.id++
    readonly type = 'variable'

    constructor(
        readonly reference: number = 0
    ){}
}

type NotebookEvent = NotebookCapabilities | InterruptRequest | ExecuteRequest | VariableRequest

interface KernelCapabilities {
    canProvideVariables?: boolean
}
interface KernelCapabilitiesEvent extends KernelCapabilities {
    type: 'capabilities'
}

interface KernelExecuted {
    type: 'executed'
    id: number
    success: boolean
    duration: number
    result: any
    showResult?: boolean
}

interface KernelOutput {
    type: 'output'
    id: number
    items: {
        mime: string
        value: string // base64
    }[]
}

export interface Variable {
    reference?: number
    name: string
    value: string
}

interface VariablesResponse {
    type: 'variables'
    id: number
    error: string
    variables: Variable[]
}

interface KernelError {
    type: 'error'
    id: number
    name?: string
    line?: number
    column?: number
    message: string
    stack?: string
}

interface KernelAbort {
    type: 'abort'
    id: number
    value: any
    line?: number
    column?: number
    stack?: string
}

type KernelExecutionEvent = KernelExecuted | KernelOutput | KernelError | KernelAbort | VariablesResponse
type KernelEvent = KernelExecutionEvent | KernelCapabilitiesEvent

export class NotebookKernelFactory {
    private kernels = new Map<string, Promise<NotebookKernel | undefined>>()
    private active: NotebookKernel | undefined
    private subOnDidStop: Disposable | undefined

    private readonly didChangeActiveKernel = new EventEmitter<NotebookKernel | undefined>()
    public readonly onDidChangeActiveKernel = this.didChangeActiveKernel.event

    private readonly disposable: Disposable[] = [this.didChangeActiveKernel]

    constructor() {
        this.updateActive(window.activeTextEditor?.document?.uri)
        this.disposable.push(window.onDidChangeActiveTextEditor(e => this.updateActive(e?.document?.uri)))
        this.disposable.push(workspace.onDidOpenNotebookDocument(e => this.updateActive(e.uri)))
    }

    dispose() {
        this.subOnDidStop?.dispose()
        this.disposable.forEach(x => x.dispose())
    }

    private async updateActive(uri?: Uri | NotebookKernel) {
        if (!uri) {
            this.active = undefined
            this.subOnDidStop?.dispose()
            this.didChangeActiveKernel.fire(undefined)
            commands.executeCommand('setContext', 'goPlaybooks.hasActiveKernel', false)
            return
        }

        const kernel = uri instanceof NotebookKernel ? uri : await this.forUri(uri, false)
        if (this.active === kernel) {
            return
        }

        this.subOnDidStop?.dispose()
        this.active = kernel
        this.subOnDidStop = kernel?.onDidStop(() => this.updateActive())
        this.didChangeActiveKernel.fire(kernel)
        commands.executeCommand('setContext', 'goPlaybooks.hasActiveKernel', !!kernel)
    }

    private async make(doc: NotebookDocument) {
        const exec = new UnixSocketProcess(doc)
        const socket = await exec.start()
        if (!socket) {
            this.kernels.delete(doc.uri.toString())
            return
        }

        const kernel = new NotebookKernel(doc, socket)
        kernel.onDidStop(() => {
            this.kernels.delete(doc.uri.toString())
            commands.executeCommand('setContext', 'goPlaybooks.kernelRunning', Array.from(this.kernels.keys()))
        })

        this.updateActive(kernel)

        commands.executeCommand('setContext', 'goPlaybooks.kernelRunning', Array.from(this.kernels.keys()))
        await kernel.start()
        return kernel
    }

    async for(doc: NotebookDocument, create = true) {
        const key = doc.uri.toString()
        if (this.kernels.has(key)) {
            return this.kernels.get(key)
        }

        if (!create) {
            return
        }

        const promise = this.make(doc)
        this.kernels.set(doc.uri.toString(), promise)
        return promise
    }

    forUri(uri: Uri, create = true) {
        for (const notebook of workspace.notebookDocuments) {
            if (notebook.notebookType != 'go-playbooks') {
                continue
            }

            if (uriBelongsToNotebook(notebook, uri)) {
                return this.for(notebook, create)
            }
        }
    }
}

function uriBelongsToNotebook(doc: NotebookDocument, uri: Uri) {
    if (doc.uri.toString() === uri.toString()) {
        return true
    }

    for (const cell of doc.getCells()) {
        if (cell.document.uri.toString() === uri.toString()) {
            return true
        }
    }

    return false
}

export class NotebookKernel {

    private readonly stream: Stream<KernelEvent, NotebookEvent>
    private readonly diagnostics = languages.createDiagnosticCollection()
    private _capabilities: KernelCapabilities = { }

    private readonly didExecute = new EventEmitter<void>()
    public readonly onDidExecute = this.didExecute.event
    private readonly didStop = new EventEmitter<void>()
    public readonly onDidStop = this.didStop.event

    private readonly disposable: Disposable[] = [this.didExecute, this.didStop]

    constructor(
        private readonly doc: NotebookDocument,
        socket: Socket
    ){
        this.stream = new Stream<KernelEvent, NotebookEvent>(socket)
    }

    async start() {
        this.disposable.push(this.stream.onClose(() => {
            this.dispose()
        }))
        this.disposable.push(workspace.onDidCloseNotebookDocument(e => {
            if (e.uri.toString() == this.doc.uri.toString()) {
                this.dispose()
            }
        }))

        this._capabilities = await new Promise<KernelCapabilities>(resolve => {
            const sub = this.stream.onEvent(callback)

            this.stream.send(new NotebookCapabilities)

            function callback(event: KernelEvent) {
                if (event.type != 'capabilities') {
                    return false
                }
                sub.dispose()
                resolve(event)
                return true
            }
        })
    }

    get capabilities() {
        return this._capabilities
    }

    async getVariables(ref: number = 0) {
        const resp = await new Promise<VariablesResponse>(resolve => {
            const sub = this.stream.onEvent(callback)

            this.stream.send(new VariableRequest(ref))

            function callback(event: KernelEvent) {
                if (event.type != 'variables') {
                    return false
                }
                sub.dispose()
                resolve(event)
                return true
            }
        })

        if (resp.error) {
            throw new Error(resp.error)
        }

        return resp.variables
    }

    dispose() {

        this.stream.destroy()
        this.didStop.fire()
        this.disposable.forEach(x => x.dispose())
    }

    async execute(cells: NotebookCell[], notebook: NotebookDocument, controller: NotebookController) {
        for (const cell of cells) {
            await this.executeCell(controller.createNotebookCellExecution(cell))
        }
    }

    async interrupt() {
        this.stream.send(new InterruptRequest)
    }

    async executeCell(execution: NotebookCellExecution) {
        // Setup promise to await completion
        type endData = { success: boolean, duration?: number }
        let resolve: (_: endData) => void
        const done = new Promise<endData>(r => (resolve = r))

        // Create the request, clear diagnostics, set the execution order
        const request = new ExecuteRequest(execution.cell.document.getText())
        this.diagnostics.set(execution.cell.document.uri, [])
        execution.executionOrder = request.id

        // Message handler
        const handler = this.stream.onEvent((event) => {
            if (!('id' in event) || event.id != request.id) {
                return false
            }

            switch (event.type) {
            case 'executed':
                const { success, duration, result, showResult } = event
                if (showResult) {
                    if (result === null) {
                        execution.appendOutput(new NotebookCellOutput([NotebookCellOutputItem.text('nil', 'text/x-go')]))
                    } else {
                        execution.appendOutput(new NotebookCellOutput([NotebookCellOutputItem.json(result)]))
                    }
                }
                resolve({ success, duration })
                return true
            case 'error':
                this.handleError(event, execution)
                return true
            case 'abort':
                this.handleAbort(event, execution)
                return true
            case 'output':
                this.handleOutput(event, execution)
                return true
            default:
                return false
            }
        })

        // Close handler
        const didClose = () => {
            resolve({ success: false })
        }
        const closeSub = this.stream.onClose(didClose)

        // Send the execution request
        const start = new Date().getTime()
        execution.start(start)
        execution.clearOutput()
        this.stream.send(request)

        // Wait for completion
        const { success, duration } = await done
        this.didExecute.fire()
        handler.dispose()
        closeSub.dispose()
        const end = duration ? start + duration*1000 : new Date().getTime()
        execution.end(success, end)
    }

    private handleAbort(event: KernelAbort, execution: NotebookCellExecution) {
        let { value } = event
        const message =
            value == null ? 'Aborted' :
            value instanceof Array ? 'Aborted: ' + value.join(' ') :
            `Aborted: ${value}`
        execution.appendOutput(new NotebookCellOutput([ NotebookCellOutputItem.text(message) ]))
        // execution.appendOutput(new NotebookCellOutput([ NotebookCellOutputItem.error({ name: 'Aborted', message: `${value}`, stack }) ]))
    }

    private handleError(event: KernelError, execution: NotebookCellExecution) {
        let { line, column, name = 'Error', message, stack } = event
        execution.appendOutput(new NotebookCellOutput([ NotebookCellOutputItem.stderr(message) ])) // Why is this necessary?
        execution.appendOutput(new NotebookCellOutput([ NotebookCellOutputItem.error({ name, message, stack }) ]))

        if (!line) {
            return
        }

        line -= 1
        if (column) {
            column -= 1
        } else {
            column = 0
        }

        const textLine = execution.cell.document.lineAt(line)
        const range = new Range(line, column, line, textLine.text.length - column)
        const diagnostic = new Diagnostic(range, message, DiagnosticSeverity.Error)
        this.diagnostics.set(execution.cell.document.uri, [diagnostic])
    }

    private handleOutput(event: KernelOutput, execution: NotebookCellExecution) {
        const output = event as KernelOutput
        const items = output.items.map((item) => {
            const value = Buffer.from(item.value, 'base64')

            switch (item.mime) {
            case 'stdout':
                return NotebookCellOutputItem.stdout(value.toString('utf-8'))
            case 'stderr':
                return NotebookCellOutputItem.stderr(value.toString('utf-8'))
            default:
                return new NotebookCellOutputItem(value, item.mime)
            }
        })
        execution.appendOutput(new NotebookCellOutput(items))
    }
}