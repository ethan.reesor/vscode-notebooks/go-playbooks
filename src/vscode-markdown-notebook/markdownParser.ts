/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See LICENSE in this folder for license information.
 *--------------------------------------------------------------------------------------------*/

import * as vscode from 'vscode';
import * as yaml from 'yaml';

export interface RawNotebookCell extends NotebookCellMetadata {
	language: string;
	content: string;
	kind: vscode.NotebookCellKind;
	output?: RawNotebookCell[];
}

export interface NotebookCellMetadata {
	indentation?: string;
	leadingWhitespace: string;
	trailingWhitespace: string;
	rawMetadata?: string;
	metadata?: { [key: string]: any };
	isFrontmatter?: boolean;
}

const LANG_IDS = new Map([
	['bat', 'batch'],
	['c++', 'cpp'],
	['js', 'javascript'],
	['ts', 'typescript'],
	['cs', 'csharp'],
	['py', 'python'],
	['py2', 'python'],
	['py3', 'python'],
]);
const LANG_ABBREVS = new Map(
	Array.from(LANG_IDS.keys()).map(k => [LANG_IDS.get(k), k])
);

interface ICodeBlockStart {
	langId: string;
	metadata?: string;
	indentation: string;
}

/**
 * Note - the indented code block parsing is basic. It should only be applied inside lists, indentation should be consistent across lines and
 * between the start and end blocks, etc. This is good enough for typical use cases.
 */
function parseCodeBlockStart(line: string): ICodeBlockStart | null {
	const match = line.match(/(    |\t)?```([^\s\{]*)(\{[^\n]*\})?/);
	return match && {
		indentation: match[1],
		langId: match[2],
		metadata: match[3],
	};
}

function isCodeBlockStart(line: string): boolean {
	return !!parseCodeBlockStart(line);
}

function isCodeBlockEndLine(line: string): boolean {
	return !!line.match(/^\s*```/);
}

export function parseMarkdown(content: string): RawNotebookCell[] {
	const parts = content.split(/^---$/mg, 3);
	let cells: RawNotebookCell[] = [];
	if (parts.length == 3 && parts[0].trim().length == 0) {
		content = parts[2];
		cells.push({
			language: 'yaml',
			content: parts[1].slice(1, parts[1].length-1),
			kind: vscode.NotebookCellKind.Code, // TODO Can we make this markup?
			leadingWhitespace: parts[0] + '---\n',
			trailingWhitespace: '\n---',
			isFrontmatter: true,
		})
	}

	const lines = content.split(/\r?\n/g);
	let i = 0;

	// Each parse function starts with line i, leaves i on the line after the last line parsed
	for (; i < lines.length;) {
		const leadingWhitespace = i === 0 ? parseWhitespaceLines(true) : '';
		if (i >= lines.length) {
			break;
		}
		const codeBlockMatch = parseCodeBlockStart(lines[i]);
		if (codeBlockMatch) {
			parseCodeBlock(leadingWhitespace, codeBlockMatch);
		} else {
			parseMarkdownParagraph(leadingWhitespace);
		}
	}

	function parseWhitespaceLines(isFirst: boolean): string {
		let start = i;
		const nextNonWhitespaceLineOffset = lines.slice(start).findIndex(l => l !== '');
		let end: number; // will be next line or overflow
		let isLast = false;
		if (nextNonWhitespaceLineOffset < 0) {
			end = lines.length;
			isLast = true;
		} else {
			end = start + nextNonWhitespaceLineOffset;
		}

		i = end;
		const numWhitespaceLines = end - start + (isFirst || isLast ? 0 : 1);
		return '\n'.repeat(numWhitespaceLines);
	}

	function parseCodeBlock(leadingWhitespace: string, codeBlockStart: ICodeBlockStart): void {
		const language = LANG_IDS.get(codeBlockStart.langId) || codeBlockStart.langId;
		const startSourceIdx = ++i;
		while (true) {
			const currLine = lines[i];
			if (i >= lines.length) {
				break;
			} else if (isCodeBlockEndLine(currLine)) {
				i++; // consume block end marker
				break;
			}

			i++;
		}

		const content = lines.slice(startSourceIdx, i - 1)
			.map(line => line.replace(new RegExp('^' + codeBlockStart.indentation), ''))
			.join('\n');
		const trailingWhitespace = parseWhitespaceLines(false);
		cells.push({
			language,
			content,
			rawMetadata: codeBlockStart.metadata,
			metadata: tryParseMetadata(codeBlockStart.metadata),
			kind: vscode.NotebookCellKind.Code,
			leadingWhitespace: leadingWhitespace,
			trailingWhitespace: trailingWhitespace,
			indentation: codeBlockStart.indentation
		});
	}

	function tryParseMetadata(s?: string): any | undefined {
		if (!s) return;

		try {
			return yaml.parse(s)
		} catch (_) {
			return
		}
	}

	function parseMarkdownParagraph(leadingWhitespace: string): void {
		const startSourceIdx = i;
		while (true) {
			if (i >= lines.length) {
				break;
			}

			const currLine = lines[i];
			if (currLine === '' || isCodeBlockStart(currLine)) {
				break;
			}

			i++;
		}

		const content = lines.slice(startSourceIdx, i).join('\n');
		const trailingWhitespace = parseWhitespaceLines(false);
		cells.push({
			language: 'markdown',
			content,
			kind: vscode.NotebookCellKind.Markup,
			leadingWhitespace: leadingWhitespace,
			trailingWhitespace: trailingWhitespace,
		});
	}

	const ids = new Map<Number, RawNotebookCell>();
	for (let i = 0; i < cells.length; i++) {
		const cell = cells[i];

		const { id, result_of } = cell.metadata || {};
		if (typeof result_of === 'number' && ids.has(result_of)) {
			ids.get(result_of)!.output!.push(cell);
			cells.splice(i, 1);
			i--;
			continue;
		}

		if (typeof id === 'number') {
			ids.set(id, cell);
		}
		cell.output = [];
	}

	for (const cell of cells) {
		if (!cell.output || !cell.output.length) {
			continue;
		}

		cell.trailingWhitespace = cell.output[cell.output.length-1].trailingWhitespace;
	}

	return cells;
}

// there are globals in workers and nodejs
declare class TextDecoder {
	decode(data: Uint8Array): string;
}

const decoder = new TextDecoder();

function stringifyMetadata(metadata?: any): string {
	if (!metadata) return '';

	function stringify(value: any): string {
		if (value == null) {
			return '~'
		}
		if (value instanceof Array) {
			return `[ ${value.map(stringify).join(', ')} ]`
		}
		if (typeof value === 'object') {
			return `{ ${Object.entries(value).map(([k, v]) => `${k}: ${stringify(v)}`).join(', ')} }`
		}
		return JSON.stringify(value)
	}

	const str = stringify(metadata)
	return str;
}

export function writeCellsToMarkdown(cells: ReadonlyArray<vscode.NotebookCellData>): string {
	var maxCellId = 0;
	for (const cell of cells) {
		const { id } = cell.metadata?.metadata || {};
		if (typeof id === 'number' && id > maxCellId) {
			maxCellId = id;
		}
	}
	maxCellId++;

	let result = '';
	for (let i = 0; i < cells.length; i++) {
		const cell = cells[i];
		if (i === 0) {
			result += cell.metadata?.leadingWhitespace ?? '';
		}

		if (i === 0 && cell.metadata?.isFrontmatter) {
			result += cell.value;
			result += getBetweenCellsWhitespace(cells, i);
			continue;
		}

		if (cell.kind !== vscode.NotebookCellKind.Code) {
			result += cell.value;
			result += getBetweenCellsWhitespace(cells, i);
			continue;
		}

		const cellMetadata = (cell.metadata || {}) as NotebookCellMetadata;
		let { id: cellId } = cellMetadata.metadata || {};
		if (cell.outputs && cell.outputs.length > 0 && typeof cellId === 'undefined') {
			cellId = maxCellId++;
			if (cellMetadata.metadata == null) {
				cellMetadata.metadata = {};
			}
			cellMetadata.metadata.id = cellId;
		}

		const indentation = cellMetadata.indentation || '';
		const languageAbbrev = LANG_ABBREVS.get(cell.languageId) ?? cell.languageId;
		const metadata = cellMetadata.rawMetadata || stringifyMetadata(cellMetadata.metadata);
		const codePrefix = indentation + '```' + languageAbbrev + metadata + '\n';
		const contents = cell.value.split(/\r?\n/g)
			.map(line => indentation + line)
			.join('\n');
		const codeSuffix = '\n' + indentation + '```';

		result += codePrefix + contents + codeSuffix;

		var maxOutId = 0;
		for (const output of cell.outputs || []) {
			const { id } = output.metadata || {};
			if (typeof id === 'number' && id > maxOutId) {
				maxOutId = id;
			}
		}
		maxOutId++;

		for (const output of cell.outputs || []) {
			let { id: outId } = output.metadata || {};
			if (typeof outId === 'undefined') {
				outId = maxOutId++;
			}

			for (const item of output.items) {
				const metadata = stringifyMetadata({ result_of: cellId, id: outId, mime: item.mime });
				const codePrefix = '\n\n' + indentation +  '```' + metadata + '\n';
				const contents = decoder.decode(item.data)
					.split(/\r?\n/g)
					.map(line => indentation + line)
					.join('\n');
				const codeSuffix = '\n' + indentation + '```';
				result += codePrefix + contents + codeSuffix;
			}
		};

		result += getBetweenCellsWhitespace(cells, i);
	}
	return result;
}


function getBetweenCellsWhitespace(cells: ReadonlyArray<vscode.NotebookCellData>, idx: number): string {
	const thisCell = cells[idx];
	const nextCell = cells[idx + 1];

	if (!nextCell) {
		return thisCell.metadata?.trailingWhitespace ?? '\n';
	}

	const trailing = thisCell.metadata?.trailingWhitespace;
	const leading = nextCell.metadata?.leadingWhitespace;

	if (typeof trailing === 'string' && typeof leading === 'string') {
		return trailing + leading;
	}

	// One of the cells is new
	const combined = (trailing ?? '') + (leading ?? '');
	if (!combined || combined === '\n') {
		return '\n\n';
	}

	return combined;
}
